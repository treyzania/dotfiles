
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Init stuff

(require 'package)

;; Fix for some dumb bug.
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))

  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)

  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives
      (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))

(package-initialize)

;; Make sure use-package is installed before doing stuff.
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; macOS is dumb with how it handles sessions so we have to manually do this :(
(when (string-equal system-type "darwin")
  ;; This one just forces us to do use modules for things since not using them
  ;; is a *really really shitty thing to do*.
  (setenv "GO111MODULE" "on")

  ;; We don't normally write darwin-specific things, like, ever, so it's safe
  ;; to assume Linux when running things from within Emacs.
  (setenv "GOOS" "linux")

  ;; This copies some other envvars from the shell on startup.
  (use-package exec-path-from-shell
    :ensure t
    :config
    (exec-path-from-shell-copy-env "GOPATH") ; y u do dis 2 me?
    (setq exec-path-from-shell-check-startup-files nil)) ; I know what I'm doing.
  (exec-path-from-shell-initialize))

;; If the dir we're actually running from is ".dotfiles", like I always do, then
;; set this to be the full path.
;; This is to help out with the gopls wrapper later on down the road.
(defvar dotdotfiles-path
  (let ((dotemacs-parent (file-name-directory
        (expand-file-name
        (file-truename load-file-name)))))
    (message ".emacs real parent is %s" dotemacs-parent)
    (if (string-equal (file-name-nondirectory (directory-file-name dotemacs-parent))
      ".dotfiles")
  dotemacs-parent
      nil)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(lsp-rust-analyzer-cargo-run-build-scripts t)
 ;'(lsp-rust-analyzer-proc-macro-enable t)
 )
 
;; Load some themes
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Core stuff

(menu-bar-mode -1)
(tool-bar-mode -1)

;(load-theme tsdh-dark)

(use-package ivy
  :ensure t)

(use-package flycheck
  :ensure t
  ;:init (global-flycheck-mode)
  ;:config
  ;(setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc))
  )

(use-package company
  :ensure t
  :custom
  (company-idle-delay 0.5)
  ;;:init (global-company-mode)
  )

(use-package neotree
  :ensure t
  :config
  (global-set-key [f8] 'neotree-toggle)
  (setq neo-theme 'nerd))

;; Custom implementation of a lsp-mode function to let us use our own language
;; servers by absolute path, if we have to.
(defun dotemacs-custom-lsp-server-present? (final-command)
  "Check whether FINAL-COMMAND is present."
  ;; executable-find only gained support for remote checks after 27 release
  (or (and (cond
            ((not (file-remote-p default-directory))
             (executable-find (cl-first final-command)))
            ((version<= "27.0" emacs-version)
             (with-no-warnings (executable-find (cl-first final-command) (file-remote-p default-directory))))
            (t))
           (prog1 t
             (lsp-log "Command \"%s\" is present on the path." (s-join " " final-command))))
      ;; This is the fun bit here.
      (and (file-name-absolute-p (cl-first final-command))
     (file-exists-p (cl-first final-command))
     (prog1 t
      (lsp-log "Command \"%s\" is present at an absolute path." (s-join " " final-command))))
      (ignore (lsp-log "Command \"%s\" is not present on the path." (s-join " " final-command)))))

(use-package lsp-mode
  :ensure t
  :hook (prog-mode . lsp)
  :commands lsp
  
  :custom
  (lsp-idle-delay 0.6)
  (lsp-rust-analyzer-server-display-inlay-hints t)
  
  :config
  ;; blah
  (setq lsp-enable-snippet nil)

  ;; Things interact poorly with this, at least on macOS.  Maybe ok on Linux.
  (setq lsp-prefer-flymake :none)

  ;; Rewrite the function to check if a server is present since we use absolute paths now.
  ;(defun lsp-server-present? (final-command)
  ;  (dotemacs-custom-lsp-server-present? final-command))

  ;; Hook in our gopls wrapper in case we need it.
  (when dotdotfiles-path
    (lsp-register-client
      (make-lsp-client :new-connection
        (lsp-stdio-connection
          (lambda ()
            (cons (concat dotdotfiles-path "gopls-cd-wrapper.sh")
              lsp-gopls-server-args)))
          :major-modes '(go-mode)
          :priority 5
          :server-id 'goplsw
          :library-folders-fn (lambda (_workspace)
              lsp-clients-go-library-directories)))))

;; TODO Make this look nicer.
;;(use-package lsp-ui
;;  :ensure t
;;  :commands lsp-ui)

;(use-package company-lsp
;  :ensure t
;  :commands company-lsp)

;; TODO Actually set this up.
(use-package tramp
  :ensure t)

(use-package docker-tramp
  :ensure t
  :config
  (push
   (cons
    "docker"
    '((tramp-login-program "docker")
      (tramp-login-args (("exec" "-it") ("%h") ("/bin/bash")))
      (tramp-remote-shell "/bin/sh")
      (tramp-remote-shell-args ("-i") ("-c"))))
   tramp-methods)
  (defadvice tramp-completion-handle-file-name-all-completions
      (around dotemacs-completion-docker activate)
    "(tramp-completion-handle-file-name-all-completions \"\" \"/docker:\" returns
    a list of active Docker container names, followed by colons."
    (if (equal (ad-get-arg 1) "/docker:")
  (let* ((dockernames-raw (shell-command-to-string "docker ps | awk '$NF != \"NAMES\" { print $NF \":\" }'"))
         (dockernames (cl-remove-if-not
           #'(lambda (dockerline) (string-match ":$" dockerline))
           (split-string dockernames-raw "\n"))))
    (setq ad-return-value dockernames))
      ad-do-it)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Rust stuff

;; "Bruh wtf use rustic" - Friend, intoxicated
(use-package rustic
  :ensure t
  :hook ((before-save . (lambda () (lsp-format-buffer))))
  :bind (:map rustic-mode-map
          ("M-j" . lsp-ui-imenu)
          ("M-?" . lsp-find-references)
          ("C-c C-c l" . flycheck-list-errors)
          ("C-c C-c a" . lsp-execute-code-action)
          ("C-c C-c r" . lsp-rename)
          ("C-c C-c q" . lsp-workspace-restart)
          ("C-c C-c Q" . lsp-workspace-shutdown)
          ("C-c C-c s" . lsp-rust-analyzer-status))
  :config
  (setq rustic-lsp-server 'rust-analyzer)
  (setq rustic-format-on-save nil)
  (setq rustic-lsp-format t)
  (setq rustic-format-display-method 'ignore)
  (define-key rustic-mode-map (kbd "TAB") #'company-indent-or-complete-common))

(use-package cargo
  :ensure t
  :after rustic-mode
  :hook (rustic-mode . cargo-minor-mode))

;; BROKEN
;;(use-package flycheck-rust
;;  :hook ((flycheck-mode . flycheck-rust-setup)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Go Stuff

;; What GOPATH was on startup.
(defvar dotemacs-original-gopath (getenv "GOPATH"))

;; Reads the ".gopath" dir in the current dir and finds the absolute path.
(defun dotemacs-dotgopath-resolve (projdir)
  (let ((dgp-path (expand-file-name ".gopath" projdir)))
    (if (file-exists-p dgp-path)
  (with-temp-buffer
    (insert-file-contents-literally dgp-path)
    (goto-char (point-min))
    (expand-file-name (string-trim (thing-at-point 'line))
          projdir))
      nil)))

(use-package go-mode
  :ensure t
  :after lsp-mode
  :hook ((before-save . gofmt-before-save)
    (go-mode . company-lsp)
    (lsp-before-initialize
     . (lambda ()
        (let ((newgopath (dotemacs-dotgopath-resolve
        (lsp--workspace-root lsp--cur-workspace))))
    (when (not (null newgopath))
      (message "Temporarily setting GOPATH to %s" newgopath)
      (setenv "GOPATH" newgopath)))))
   ;(lsp-after-initialize
   ; . (lambda () (setenv "GOPATH" dotemacs-original-gopath)))
   )
  :config
  (define-key go-mode-map (kbd "TAB") #'company-indent-or-complete-common))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; C stuff

(use-package cc-mode
  :ensure t
  :hook ((prog-mode . (lambda () (interactive)
      (setq show-trailing-whitespace 1)))
   (c-mode . (lambda ()
         (define-key c-mode-base-map (kbd "<tab>") 'company-indent-or-complete-common)))))

(use-package company-c-headers
  :ensure t
  :config
  (add-to-list 'company-backends 'company-c-headers))

(add-hook 'c-mode-common-hook 'hs-minor-mode)
(setq c-default-style "linux")
(setq c-basic-offset 4)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; OCaml

;; Set the Emacs source path for OPAM things.
(defvar opam-site-path
  (concat (getenv "HOME")
    "/.opam/4.09.0/share/emacs/site-lisp"))


;; idrk what this does
(push opam-site-path load-path)

(use-package tuareg
  :ensure t)

;(use-package ocamlformat
;  :hook
;  (tuareg-mode . (lambda ()
;       (define-key tuareg-mode-map (kbd "C-M-<tab>") #'ocamlformat)
;       (add-hook 'before-save-hook #'ocamlformat-before-save))))

(autoload 'merlin-mode "merlin" "Merlin mode" t)
(add-hook 'tuareg-mode-hook 'merlin-mode)
(add-hook 'caml-mode-hook 'merlin-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Other formats

;; Meson
(use-package meson-mode
  :ensure t
  :hook (meson-mode . company-mode))

;; Protobuf
(use-package protobuf-mode
  :ensure t)

;; Yaml
(use-package yaml-mode
  :ensure t)

;; JSON
(use-package json-mode
  :ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Other stuff

;; Make indents prettier
(setq-default indent-tabs-mode nil)
(setq tab-width 4)

;; which key just runs everywhere
(use-package which-key
  :ensure t)
(which-key-mode)

;; Column numbers!
(setq column-number-mode t)

;; Nicer navigation.
(global-set-key (kbd "C-<PageUp>") 'windmove-left)
(global-set-key (kbd "C-<PageDown>") 'windmove-right)

;; Smoother scrolling
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ; one line at a time
(setq mouse-wheel-progressive-speed nil) ; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ; scroll window under mouse
(setq scroll-step 1) ; keyboard scroll one line at a time

(setq calendar-latitude 42)
(setq calendar-longitude -71)
(put 'scroll-left 'disabled nil)

(setq custom-file (substitute-in-file-name "$HOME/.emacs.d/.custom.el"))
;; ## added by OPAM user-setup for emacs / base ## 56ab50dc8996d2bb95e7856a6eddb17b ## you can edit, but keep this line
(when (file-exists-p "~/.emacs.d/opam-user-setup.el")
  (require 'opam-user-setup "~/.emacs.d/opam-user-setup.el"))
;; ## end of OPAM user-setup addition for emacs / base ## keep this line
