#!/bin/bash

# Filename of the flag to say "hey cd to somewhere that's not here".
GMP_FILENAME='.gomodpath'

# Give a warning to people:
#   "when will you learn?
#    when will you learn?
#    that your actions have consequences!"
echo -e "I would like to take this opportunity to remind you that Go sucks, and you shouldn't use it.\n" 1>&2

# Here we check if there's a file called ".gomodpath", and if there is then we
# read it and cd to that directory.
if [ -f $GMP_FILENAME ]; then
	gmp=$(cat $GMP_FILENAME)
	echo 'switching directory to' $gmp 1>&2
	export GO111MODULE=on
	cd $gmp
fi

# Let's dump the go env here, just to figure out what we're really looking at.
go env 1>&2

# And now just exec gopls, our work here (if we did any) is done and we don't
# need to exist anymore.
exec gopls $@
