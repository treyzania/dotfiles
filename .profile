# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# So that we're sure we ran this file.
export TZ_DOT_PROFILE_RUN=yup

# if running bash
#if [ -n "$BASH_VERSION" ] && [ -z "$TZ_DOT_BASHRC_RUN" ]; then
#    # include .bashrc if it exists
#    if [ -f "$HOME/.bashrc" ]; then
#	. "$HOME/.bashrc"
#    fi
#fi

# Cargo env
if [ -f $HOME/.cargo/env ]; then
	. $HOME/.cargo/env
	# Do toolchains?
fi

# Go is a shitty language
if [ -n $(command -v go) ]; then
	export GOPATH="$HOME/.go"
	export PATH="$GOPATH/bin:$PATH"
fi

# OPAM configuration
if [ -d ~/.opam ]; then
	. $HOME/.opam/opam-init/init.sh > /dev/null 2> /dev/null || true
fi

# Maven
MVNDIR=/opt/apache-maven-3.6.1
if [ -d $MVNDIR ]; then
	export PATH="$MVNDIR/bin:$PATH"
fi

# And just the private ~/bin
if [ -d ~/bin ]; then

	export PATH="$HOME/bin:$PATH"

	# Gradle
	# TODO Move this somewhere better.
	GRADLEDIR=$HOME/bin/gradle-4.7
	if [ -d $GRADLEDIR ]; then
		export PATH="$GRADLEDIR/bin:$PATH"
	fi

fi

# dotfiles-local bin
if [ -d ~/.dotfiles ]; then
	export PATH="$PATH:$HOME/.dotfiles/bin"
fi

if [ -d ~/.local/bin ]; then
	export PATH="$HOME/.local/bin:$PATH"
fi

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
